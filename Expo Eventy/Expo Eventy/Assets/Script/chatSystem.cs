﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class chatSystem : MonoBehaviour {

	public GameObject chatPlayer, chatBot, loadingChat;

    public GameObject missChatP, missChatB;
	public GameObject playerChat, botChat;
	public GameObject chatMenu;
    public Button sendBtn;

	public InputField textInput;
    public Text yourText, botText;

    //string playerMessage, botMessage;

    float waitBotSend;
    bool startCounting;

	void Update()
    {   
        if (Input.GetKeyDown(KeyCode.Minus))
        {
            chatMenu.SetActive(true);
        }

        if (chatMenu.activeInHierarchy == false)
        {
            for (int i = playerChat.transform.childCount - 1; i >= 0; i--)
            {
                Destroy(playerChat.transform.GetChild(i).gameObject);
            }
        }

        if (waitBotSend >= 20)
        {
            botSendMessage();
            waitBotSend = 0;
        }
        else
        {
            if (startCounting)
            {
                waitBotSend++;
            }
        }
    }

    void botSendMessage() 
    {
        GameObject chatGo = Instantiate(chatBot, transform.position, Quaternion.identity, botChat.transform);
        chatGo.transform.GetChild(0).GetComponent<Text>().text = "Hai";

        GameObject dumpGo = Instantiate(loadingChat, transform.position, Quaternion.identity, botChat.transform);
        Destroy(dumpGo, .1f);

        GameObject missGoP = Instantiate(missChatP, transform.position, Quaternion.identity, playerChat.transform);
        missGoP.GetComponent<Image>().enabled = false;

        GameObject dumpGo2 = Instantiate(loadingChat, transform.position, Quaternion.identity, playerChat.transform);
        Destroy(dumpGo2, .1f);
        startCounting = false;
    }

	public void sendMessage()
    {                
		GameObject chatGo = Instantiate(chatPlayer, transform.position, Quaternion.identity, playerChat.transform);
		chatGo.transform.GetChild(0).GetComponent<Text>().text = textInput.text;      

        GameObject dumpGo = Instantiate(loadingChat, transform.position, Quaternion.identity, playerChat.transform);
        Destroy(dumpGo, .1f);

        GameObject missGo = Instantiate(missChatB, transform.position, Quaternion.identity, botChat.transform);
        missGo.transform.GetChild(0).GetComponent<Text>().text = textInput.text;

        GameObject dumpGo2 = Instantiate(loadingChat, transform.position, Quaternion.identity, botChat.transform);
        Destroy(dumpGo2, .1f);
        textInput.text = "";
        startCounting = true;
        StartCoroutine(moveTextEnd_NextFrame());
    }

    void OnGUI()
    {
        if (textInput.isFocused && textInput.text != "" && Input.GetKeyDown(KeyCode.Return))
        {
            sendMessage();
            //textInput.text = "";
        }
        else if (textInput.isFocused == false  && textInput.text == "" && Input.GetKeyDown(KeyCode.Return))
        {
            textInput.Select();
        }
        else if (textInput.isFocused && textInput.text != "")
        {
            sendBtn.interactable = true;
        }
        else if (!textInput.isFocused && textInput.text == "")
        {
            sendBtn.interactable = false;
        }
    }

    IEnumerator moveTextEnd_NextFrame()
    {
        yield return 1;
        textInput.MoveTextEnd(false);        
    }
}
