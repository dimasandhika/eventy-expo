﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeLookCamera : MonoBehaviour {

    

    public enum GameView { playerView, spectactingView}
    public GameView gameView = GameView.playerView;
    
    public GameObject playerObject, spectObject;
    public float speed, rotationSpeed;
    float mouseX, mouseY;
    bool move, isMoving;
    Camera spectCamera;

    void Start()
    {
        spectCamera = spectObject.GetComponentInChildren<Camera>();
        isMoving = true;
    }

    void Update()
    {  
        float d = Input.GetAxis("Mouse ScrollWheel");

        if (d > 0f && spectCamera.fieldOfView > 2)
        {
            spectCamera.fieldOfView -= 2;
        }
        else if (d < 0f && spectCamera.fieldOfView < 100)
        {
            spectCamera.fieldOfView += 2;
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            changeView();
        }

        if (move)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                isMoving = !isMoving;
            }

            if (isMoving)
            {
                spectacting();
            }
            else
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        }
    }

    void changeView()
    {
       

        switch (gameView)
        {
            case GameView.playerView:
                switchView(true);                
                gameView = GameView.spectactingView;

                break;
            case GameView.spectactingView:
                switchView(false);
                gameView = GameView.playerView;
                break;
            default:
                break;
        }
    }

    void switchView(bool isActive)
    {
        playerObject.SetActive(isActive);
        playerObject.GetComponentInParent<thirdPersonCharacterController>().enabled = isActive;
        spectObject.SetActive(!isActive);
        move = !isActive;
    }

	void spectacting()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        //object Move
        float hor = Input.GetAxis("Horizontal");
        float ver = Input.GetAxis("Vertical");

        Vector3 spectatctingMove;
        if (Input.GetKey(KeyCode.LeftShift))
        {
            spectatctingMove = new Vector3(hor, ver, 0f) * speed;
        }
        else
        { 
            spectatctingMove = new Vector3(hor, 0f, ver) * speed;
        }
        transform.Translate(spectatctingMove, Space.Self);

        //camControl
        mouseX += Input.GetAxis("Mouse X") * rotationSpeed;
        mouseY -= Input.GetAxis("Mouse Y") * rotationSpeed;
        mouseY = Mathf.Clamp(mouseY, -30, 60);

        transform.rotation = Quaternion.Euler(mouseY, mouseX, 0f);
    }

}
