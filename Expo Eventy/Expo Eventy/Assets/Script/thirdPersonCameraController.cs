﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class thirdPersonCameraController : MonoBehaviour {

    [Range(1, 10f)]
	public float rotationSpeedMouse;
    public float rotationSpeedPlayer;
    //public float moveSpeedNum;
    public Transform target, player, pointer;
    [HideInInspector]
    public float mouseX, mouseY, playerX;
    public GameObject panelInMenu;

    public Transform obstruction;

    public bool moveCam = true;
       
    public Vector3 moveSpeed;

    public Camera mainCamera;

    public LayerMask whatIsPlayer;

    bool moveLeft, moveRight;

    private void Awake() 
    {
         if (PlayerPrefs.HasKey("MouseSetting"))
        {
            rotationSpeedMouse = PlayerPrefs.GetFloat("MouseSetting");
        }
        else
        {
            rotationSpeedMouse = 4;
            PlayerPrefs.SetFloat("MouseSetting", rotationSpeedMouse);
        }
    }

    void Start()
    {                      
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
       
    }

    void Update()
    {       
        if (panelInMenu.activeInHierarchy == true)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            moveCam = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            moveCam = true;
        }
    }
    
    void LateUpdate()
    {        
        //Pengaturan kamera
        if (moveCam)
        {            
            CamControll();
        }
         //   viewObstructed();
    }

    //Pergerakan kamera
    void CamControll()
    {       
        //Ray myRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        //RaycastHit mouseHit;

        mouseX += Input.GetAxis("Mouse X") * rotationSpeedMouse;
        mouseY -= Input.GetAxis("Mouse Y") * rotationSpeedMouse;
        playerX = mouseX * rotationSpeedPlayer;
        mouseY = Mathf.Clamp(mouseY, -35, 60);
        //transform.LookAt(target);
      
        target.rotation = Quaternion.Euler(mouseY, mouseX, 0);
        player.transform.rotation = Quaternion.Euler(0, mouseX, 0);
        //pointer.rotation = Quaternion.Euler(mouseY, 0, 0);
        
        //Vector3 newDir = Vector3.RotateTowards(transform.forward, pointer.position, rotationSpeed, 0f);

        //Bergerak dengan cara klik mouse
        #region MouseMovement
        //if (Physics.Raycast(myRay, out mouseHit, 100, whatIsSideLeft))
        //{
        //    //Player.rotation = Quaternion.Euler(0f, mouseX, 0f);
        //    //target.rotation = Quaternion.Euler(mouseY, mouseX, 0f);
        //    Player.transform.Rotate(0f, -rotationSpeed * Time.deltaTime, 0f);        
        //}
        //else if (Physics.Raycast(myRay, out mouseHit, 100, whatIsSideRight))
        //{
        //    Player.transform.Rotate(0f, rotationSpeed * Time.deltaTime, 0f);
        //}

        //Menggerakkan pandangan kamera dari sisi kanan player menuju kiri player dan sebaliknya
        //if (moveLeft)
        //{
        //    transform.Translate(Vector3.left * moveSpeedNum * Time.deltaTime);
        //}

        //if (moveRight)
        //{
        //    transform.Translate(Vector3.left * -moveSpeedNum * Time.deltaTime);
        //}    

        //if (transform.localPosition.x <= -2)
        //{
        //    moveLeft = false;
        //    transform.localPosition = new Vector3(-2, transform.localPosition.y, transform.localPosition.z);
        //}
        //else if (transform.localPosition.x >= 2)
        //{
        //    moveRight = false;
        //    transform.localPosition = new Vector3(2, transform.localPosition.y, transform.localPosition.z);
        //}

        //if (Physics.Raycast(myRay, out mouseHit, 100, whatIsPlayer))
        //{            
        //    if (transform.localPosition.x >= 2)
        //        moveLeft = true;
        //    else if (transform.localPosition.x <= -2)
        //        moveRight = true;

        //}        
        #endregion
    }

    //Fungsi menghilangkan dinding selama kamera bertabrakan dengan dinding
    void viewObstructed()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, target.position - transform.position, out hit, 4.5f))
        {
            if (hit.collider.gameObject.tag == "Wall")
            {
                obstruction = hit.transform;
                obstruction.gameObject.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
            }
            else
            {
                obstruction.gameObject.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;    
            }
        }      
    }
}
