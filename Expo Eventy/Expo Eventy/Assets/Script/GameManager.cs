﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public string playTimes;

	public GameObject tutorBg;

	public GameObject[] panelPage;
    public Button[] leftRightBtn;
    public GameObject panelInMenu;

	int index;

    public Slider mouseSent;
    public float maxValue, minValue;

    public Button confirmBtn, exitBtn;
    public Button closeTutor, closeSetting;
    public Text info;
    public Text valueTxt;

    public GameObject gameMap;
    bool mapActive = false;

	void Start()
    {
        if (PlayerPrefs.HasKey("showTutor"))
        {
            playTimes = PlayerPrefs.GetString("showTutor");
            exitBtn.gameObject.SetActive(true);
            closeTutor.gameObject.SetActive(false);
            closeSetting.gameObject.SetActive(true);
        }
        else
        {
            panelInMenu.SetActive(true);
            tutorBg.SetActive(true);
            panelPage[index].SetActive(true);
            exitBtn.gameObject.SetActive(false);
            closeSetting.gameObject.SetActive(false);
            closeTutor.gameObject.SetActive(true);
            if (index == panelPage.Length - 1)
            {
                leftRightBtn[1].interactable = false;
                leftRightBtn[0].interactable = true;
                index = panelPage.Length - 1;
            }
            else if (index == 0)
            {
                leftRightBtn[0].interactable = false;
                leftRightBtn[1].interactable = true;
                index = 0;
            }
        }

        mouseSent.maxValue = maxValue;
        mouseSent.minValue = minValue;

        mouseSent.value = PlayerPrefs.GetFloat("MouseSetting");
    }

    void Update()
    {       
        gameMap.SetActive(mapActive);
        valueTxt.text = mouseSent.value.ToString();

        if (mouseSent.value != PlayerPrefs.GetFloat("MouseSetting"))
        {
            confirmBtn.gameObject.SetActive(true);
            info.gameObject.SetActive(false);
        }
        else
        {
            confirmBtn.gameObject.SetActive(false);
            info.gameObject.SetActive(true);
        }

        if (panelInMenu.activeInHierarchy == true)
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                buttonTab("Previous");
            }
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                buttonTab("Next");
            }
        }

    }

    void changeFunction(bool isActive, int position)
    {        
        switch (position)
        {
            case 1:
                confirmBtn.gameObject.SetActive(isActive);
                info.gameObject.SetActive(!isActive);
                confirmBtn.gameObject.SetActive(!isActive);
                info.gameObject.SetActive(isActive);
                break;
            case 2:
                leftRightBtn[1].interactable = !isActive;
                leftRightBtn[0].interactable = isActive;
                break;
        }
    }

    public void buttonTab(string _buttonType)
    {        
        for (int i = 0; i < panelPage.Length; i++)
        {
            panelPage[i].SetActive(false);
        }

        switch (_buttonType)
        {
            case "Next":
                index++;
                break;
            case "Previous":
                index--;
                break;
        }

        panelPage[index].SetActive(true);

        if (index == panelPage.Length - 1)
        {
            leftRightBtn[1].interactable = false;
            leftRightBtn[0].interactable = true;
            index = panelPage.Length - 1;
        }
        else if (index == 0)
        {
            leftRightBtn[0].interactable = false;
            leftRightBtn[1].interactable = true;
            index = 0;
        }
        else
        {
            leftRightBtn[0].interactable = true;
            leftRightBtn[1].interactable = true;
        }
    }

    public void functionButton(string _buttonType)
    {        
        switch (_buttonType)
        {
            case "CloseTutor":
                tutorBg.SetActive(false);
                panelInMenu.SetActive(false);
                exitBtn.gameObject.SetActive(true);
                closeSetting.gameObject.SetActive(true);
                closeTutor.gameObject.SetActive(false);
                break;
            case "Confirm":
                PlayerPrefs.SetFloat("MouseSetting", mouseSent.value);
                break;
            case "Quit":
                SceneManager.LoadScene(0);
                break;
        }
        playTimes = "Already play";
        PlayerPrefs.SetString("showTutor", playTimes);
    }
}
