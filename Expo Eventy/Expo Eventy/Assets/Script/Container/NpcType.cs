﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NpcType 
{
    public string nameNpc;
    public Sprite photoProfile;
    
}
