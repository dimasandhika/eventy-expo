﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class BoothType {

    public string nameBooth;
    public string nameNpc;
    public string url;

    public Sprite kartuNama;
    public Sprite information;
    public Sprite[] brosurs;    
    



}
