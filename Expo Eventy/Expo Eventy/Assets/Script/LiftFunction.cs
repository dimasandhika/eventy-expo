﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LiftFunction : MonoBehaviour 
{    

    bool isMoving, isAnimated;
    GameObject player;
    public Camera liftCamera, playerCamera;
    public float defaultSpeed, playerSpeed, checkFloor;
    private float speed;
    public GameObject HUD, liftTrigger, wayPointIn, wayPointOut;

    int animatedLiftStep, childIndex;

    //string liftCond;
    public Transform spwanPosDown, spawnPosUp, spawnInside;

    bool isTransition;
    public Animator transisiAnim;
    float delayTime;
    public float time;
    void Start()
    {
        speed = defaultSpeed;
        childIndex = transform.childCount;
    }

    void Update()
    {

        transisiAnim.SetBool("isTransition", isTransition);
        if (playerCamera.gameObject.activeInHierarchy == false)
        {
            HUD.SetActive(false);
            HUD.GetComponent<Animator>().enabled = false;
        }
        else
        {
            HUD.SetActive(true);
            HUD.GetComponent<Animator>().enabled = true;
        }

        //Realtime lift
        #region realtimeLift;
        /*
        if (Input.GetKeyDown(KeyCode.I))
        {
            transisiImage.SetTrigger("StartTransisi");
        }            

        if (transform.childCount > childIndex)
        {
            player = transform.GetChild(transform.childCount -1).gameObject;
        }
        else
        {
            player = null;            
        }        

        switch (animatedLiftStep)
        {
            case -2:
                player = null;
                transform.GetChild(transform.childCount - 1).transform.parent = null;
                transform.position = new Vector3(transform.position.x, transform.position.y + speed * Time.deltaTime, transform.position.z);
                break;
            case -1:
                HUD.SetActive(false);
                playerCamera.GetComponent<thirdPersonCameraController>().enabled = false;
                player.GetComponent<thirdPersonCharacterController>().playerMove = false;
                player.GetComponent<Rigidbody>().isKinematic = true;
                player.GetComponent<Rigidbody>().useGravity = false;
                animatedLiftStep = -2;
                break;
            case 0:
                if (player != null)
                {
                    if (speed != checkFloor)
                    {
                        animatedLiftStep = -1;
                    }
                    else
                    {
                        animatedLiftStep = 1;
                    }
                }
                break;
            case 1:
                liftCamera.enabled = true;
                playerCamera.enabled = false;
                HUD.SetActive(false);
                playerCamera.GetComponent<thirdPersonCameraController>().enabled = false;
                player.GetComponent<thirdPersonCharacterController>().playerMove = false;
                player.GetComponent<Rigidbody>().isKinematic = true;
                player.GetComponent<Rigidbody>().useGravity = false;                
                player.transform.position = Vector3.MoveTowards(player.transform.position, wayPointIn.transform.position, playerSpeed * Time.deltaTime);
                if (player.transform.position == wayPointIn.transform.position)
                {
                    player.GetComponent<Animator>().SetBool("isMoving", false);
                    player.GetComponent<Animator>().SetFloat("ver", 0f);
                    player.GetComponent<Animator>().SetFloat("hor", 0f);
                    playerCamera.GetComponent<thirdPersonCameraController>().enabled = true;
                    playerCamera.GetComponent<thirdPersonCameraController>().mouseX += 180f;
                    animatedLiftStep = 2;
                }
                else
                {
                    player.GetComponent<Animator>().SetBool("isMoving", true);
                    player.GetComponent<Animator>().SetFloat("ver", .5f);                    
                    player.GetComponent<Animator>().SetFloat("hor", 0f);                    
                }                
                //print("work1");
                break;
            case 2:
                playerCamera.GetComponent<thirdPersonCameraController>().enabled = false;
                player.GetComponent<thirdPersonCharacterController>().playerMove = false;
                transform.position = new Vector3(transform.position.x, transform.position.y + speed * Time.deltaTime, transform.position.z);                
                //print("work2");
                break;
            case 3:
                player.GetComponent<thirdPersonCharacterController>().playerMove = false;
                //playerCamera.GetComponent<thirdPersonCameraController>().enabled = true;
                //playerCamera.GetComponent<thirdPersonCameraController>().mouseX += 180f;
                animatedLiftStep = 4;
                //print("work3");
                break;
            case 4:
                player.GetComponent<thirdPersonCharacterController>().playerMove = false;
                //playerCamera.GetComponent<thirdPersonCameraController>().enabled = false;                
                player.transform.position = Vector3.MoveTowards(player.transform.position, wayPointOut.transform.position, playerSpeed * Time.deltaTime);
                if (player.transform.position == wayPointOut.transform.position)
                {
                    player.GetComponent<Animator>().SetBool("isMoving", false);
                    player.GetComponent<Animator>().SetFloat("ver", 0f);
                    player.GetComponent<Animator>().SetFloat("hor", 0f);
                    animatedLiftStep = 5;
                }
                else
                {
                    player.GetComponent<Animator>().SetBool("isMoving", true);
                    player.GetComponent<Animator>().SetFloat("ver", .5f);
                    player.GetComponent<Animator>().SetFloat("hor", 0f);
                }                
                //print("work4");
                break;
            case 5:                        
                animatedLiftStep = 6;
                //print("work5");
                break;
            case 6:
                if (player != null)
                {
                    liftCamera.enabled = false;
                    playerCamera.enabled = true;
                    playerCamera.transform.gameObject.GetComponent<thirdPersonCameraController>().enabled = true;
                    HUD.SetActive(true);
                    player.GetComponent<Rigidbody>().isKinematic = false;
                    player.GetComponent<Rigidbody>().useGravity = true;
                    player = null;
                    transform.GetChild(transform.childCount -1).transform.parent = null;
                    //animatedLiftStep = 0;
                }
                else
                {
                    animatedLiftStep = 0;
                }
                break;                
        }
        */
        #endregion

        //loading screen lift
        #region loadingScreenLift        
        if (transform.childCount > childIndex)
        {
            player = transform.GetChild(transform.childCount - 1).gameObject;
            player.GetComponent<thirdPersonCharacterController>().playerMove = false;
            player.GetComponent<thirdPersonCharacterController>().enabled = false;
            player.GetComponent<Animator>().SetBool("isMoving", false);
            player.GetComponent<Animator>().SetFloat("ver", 0f);
            player.GetComponent<Animator>().SetFloat("hor", 0f);            
            playerCamera.GetComponent<thirdPersonCameraController>().mouseX += 180f;
            playerCamera.GetComponent<thirdPersonCameraController>().enabled = false;
            HUD.SetActive(false);
        }
        else
        {
            player = null;
        }                           

        #endregion

        //Fast lift animation
        #region Fast lift animation
        /*
        if (transform.childCount > childIndex)
        {
            player = transform.GetChild(transform.childCount - 1).gameObject;
        }
        else
        {
            player = null;
        }

        if (player != null)
        {
            if (checkFloor == 0)
            {
                player.transform.position = spawnPosUp.position;
            }
            else if (checkFloor == 1)
            {
                player.transform.position = spwanPosDown.position;
            }

            if (player.transform.position == spawnPosUp.position || player.transform.position == spwanPosDown.position)
            {
                player.transform.parent = null;
                player = null;
            }
        }*/
        #endregion
    }

    //Lift up and down function
    void OnTriggerEnter(Collider other)
    {
        if (other.name == "Floor 0")
        {
            if (animatedLiftStep == -1)
            {
                animatedLiftStep = 1;
                speed = defaultSpeed;
            }
            else
            {
                animatedLiftStep = 3;
                speed = defaultSpeed;
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.name == "Floor 1")
        {
            if (speed == defaultSpeed)
            {
                if (animatedLiftStep == -1)
                {
                    animatedLiftStep = 1;
                    speed = defaultSpeed;
                }
                else
                {
                    animatedLiftStep = 3;
                    speed = -defaultSpeed;
                }
            }
        }
    }

    //For call animation once
    public void callAnimation()
    {        
        StartCoroutine(loadingLift());
     
    }

    //Loading process
    IEnumerator loadingLift()
    {        
        isTransition = true;
        yield return new WaitForSeconds(2f);
        
        playerCamera.enabled = false;
        liftCamera.enabled = true;
        player.transform.position = spawnInside.transform.position;
        isTransition = false;        
      
        yield return new WaitForSeconds(5f);
        isTransition = true;
        
        yield return new WaitForSeconds(2f);
        if (checkFloor == 0)
        {
            player.transform.position = spawnPosUp.position;
        }
        else if (checkFloor == 1)
        {
            player.transform.position = spwanPosDown.position;
        }
        isTransition = false;
        player.GetComponent<thirdPersonCharacterController>().playerMove = true;
        player.GetComponent<thirdPersonCharacterController>().enabled = true;
        player.GetComponent<Animator>().enabled = true;
        playerCamera.GetComponent<thirdPersonCameraController>().enabled = true;        
        HUD.SetActive(true);
        playerCamera.enabled = true;
        liftCamera.enabled = false;
        player.transform.parent = null;               
        yield break;
    }  
}
