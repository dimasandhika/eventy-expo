﻿using UnityEngine.Audio;
using System;
using UnityEngine;

public class AudioManager : MonoBehaviour {

	public Sound[] sounds;


	// Use this for initialization
	void Awake ()
	{
        foreach (Sound s in sounds)
        {
			s.source = gameObject.AddComponent<AudioSource>();
			s.source.clip = s.clip;
			s.source.volume = s.volume;
			s.source.pitch = s.pitch;
			s.source.loop = s.loop;
        }
	}


	public void Play(string _name)
    {
		Sound s = Array.Find(sounds, sound => sound.name == _name);
        if (s == null)
        {
			Debug.LogWarning("Song: " + _name + " not found!");
			return;
        }

		s.source.Play(); 
    }
}
