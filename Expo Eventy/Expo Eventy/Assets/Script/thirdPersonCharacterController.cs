﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Video;
using UnityEngine.UI;

public class thirdPersonCharacterController : MonoBehaviour {

    [Header("PlayerInteraction")]
    public string playerInteraction;
    private string boothName;
    private string objectName;


    [Header("PlayerData")]
    public GameObject panelInMenu;

	public float speed, speedRot;
    bool inRange, inBooth, inLift;

    public bool playerMove;
    public LayerMask whatIsGround, objectThatCanBeClickedOn;

    private NavMeshAgent myAgent;

    public bool inMenu = false;
    GameObject[] clickableGo;

    public GameObject clickPoint;    

    //bool playVideo;

    Vector3 mousePos;
    Vector3 worldPos;

    public Image signImg;
    public Text signTxt;
    public float upPos, distClick;    

    public GameObject lift;
    public Camera myCamera;
    
    public Animator myAnim;

    public Animator interactAnim;

    public GameObject map1F, map2F;

    void Start()
    {
        myAgent = GetComponent<NavMeshAgent>();        
        clickableGo = GameObject.FindGameObjectsWithTag("objectClick");
        myAgent.speed = speed;
        inRange = false;            
    }
    
	void Update()
    {
        // if (panelInMenu.activeInHierarchy == true)
        // {
        //     inMenu = true;
        // }
        // else
        // {
        //     inMenu = false;
        // }

        inMenu = panelInMenu.activeInHierarchy;

        #region Animasi player saat berjalan menggunakan kursor
        //Vector3 imgPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y + upPos, Input.mousePosition.z);
        //signImg.transform.position = imgPos;

        //float velocity = myAgent.velocity.magnitude;

        //myAnim.SetFloat("mag", velocity);
        //myAnim.SetFloat("mag", Input.GetAxis("Horizontal"));
        #endregion

        if (playerMove)
        {
            PlayerMovement();
        }
     
        if (inMenu == false)
        {

            if (transform.position.y < 25f)
            {
                SwitchMap(true);
            }
            else if(transform.position.y > 25f)
            {
                SwitchMap(false);
            }
            

            //Player interaksi
            playerMove = true;
            panelInMenu.SetActive(false);
            if (myCamera.enabled == true)
            {
                Ray myRay = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hitInfo;
   
                //Menggerakan player menggunakan mouse dengan cara klik pada tanah yang ingin dituju
                #region MouseMovement
                //if (Physics.Raycast(myRay, out hitInfo, 100, whatCanBeClickedOn))
                //{    
                //    //tujuan player
                //    if (Input.GetMouseButtonDown(0))
                //    {
                //        myAgent.SetDestination(hitInfo.point);
                //        myAgent.speed = speed;

                //        //spawn Point
                //        Vector3 objectPos = new Vector3(hitInfo.point.x, hitInfo.point.y + .1f, hitInfo.point.z);
                //        Quaternion spawnRot = Quaternion.Euler(90f, 0f, 0f);
                //        GameObject particle = Instantiate(clickPoint, objectPos, spawnRot);
                //        Destroy(particle, 1);
                //    }                
                //}         
                #endregion

                //Kondisi dimana player memeriksa objek
                if (Physics.Raycast(myRay, out hitInfo, distClick, objectThatCanBeClickedOn))
                {                    
                    signTxt.text = hitInfo.transform.gameObject.name;
                    signImg.gameObject.SetActive(true);
                    interactAnim.SetBool("isInteract", true);


                    if(hitInfo.transform.parent != null)
                    {
                        boothName = hitInfo.transform.parent.gameObject.name;
                        objectName = hitInfo.transform.gameObject.name;;
                        playerInteraction = boothName + "/" + objectName;
                    }
                    else if(hitInfo.transform.parent == null)
                    {
                        boothName = null;
                        objectName = hitInfo.transform.gameObject.name;
                        playerInteraction = objectName;
                    }

                    //objek yang di klik oleh player
                    for (int i = 0; i < clickableGo.Length; i++)
                    {
                        if (inRange)
                        {
                            if (!hitInfo.transform.parent)
                            {
                                if (Input.GetMouseButtonDown(0))
                                {
                                    FindObjectOfType<AudioManager>().Play("Click");
                                    showProfile(hitInfo.transform.GetChild(0).transform.gameObject.name);
                                }
                            }
                        }

                        if (inBooth)
                        {
                            if (Input.GetMouseButtonDown(0))
                            {
                                FindObjectOfType<AudioManager>().Play("Click");
                                if (hitInfo.transform.parent)
                                {
                                    showPanel(hitInfo.transform.parent.gameObject.name, hitInfo.transform.gameObject.name);
                                    if (hitInfo.transform.gameObject.name == "Video")
                                    {
                                        GameObject myVideo = hitInfo.transform.gameObject;
                                        myVideo.GetComponent<VideoPlayer>().enabled = false;
                                        myVideo.transform.GetChild(0).gameObject.SetActive(false);
                                    }
                                }
                            }
                        }

                        if (inLift)
                        {
                            if (Input.GetMouseButtonDown(0))
                            {
                                if (hitInfo.transform.gameObject.name == "Lift")
                                {
                                    if (transform.position.y < 25f)
                                    {
                                        lift.GetComponent<LiftFunction>().checkFloor = 0;
                                    }
                                    else if (transform.position.y > 25f)
                                    {
                                        lift.GetComponent<LiftFunction>().checkFloor = 1;
                                    }
                                    transform.parent = lift.transform;
                                    lift.GetComponent<LiftFunction>().callAnimation();
                                }                             
                            }
                        }
                    }
                }
                else
                {
                    playerInteraction = null;
                    boothName = null;
                    objectName = null;
                    signImg.gameObject.SetActive(false);
                    interactAnim.SetBool("isInteract", false);
                }
                //menggerakkan maincamera
                //FindObjectOfType<thirdPersonCameraController>().moveCam = true;
            }
        }
        else
        {
            playerMove = false;
            myAnim.SetBool("isMoving", false);
            myAnim.SetFloat("ver", 0f);
            myAnim.SetFloat("hor", 0f);
            panelInMenu.SetActive(true);
            //FindObjectOfType<thirdPersonCameraController>().moveCam = false;
            signImg.gameObject.SetActive(false);
            interactAnim.SetBool("isInteract", false);
        }
        //setting cursor
        //Cursor.lockState = CursorLockMode.None;
    }

    void SwitchMap(bool _isActive)
    {
        map1F.SetActive(_isActive);
        map2F.SetActive(!_isActive);
    }

    //apa yang player klik
    void showPanel(string boots, string typePanel)
    {
        if (typePanel == "Video")
        {
            inMenu = false;
        }
        else if (typePanel == "Eventy")
        {
            inMenu = false;
            FindObjectOfType<menuControl>().showPanel(boots, typePanel);
        }
        else if (typePanel == "TriggerLift")
        {
            inMenu = false;
        }
        else
        {
           inMenu = true;
           FindObjectOfType<menuControl>().showPanel(boots, typePanel);
        }
    }

    void showProfile(string _name)
    {
        inMenu = true;
        FindObjectOfType<menuControl>().showProfile(_name);
    }

    void PlayerMovement()
    {
        float hor = Input.GetAxis("Horizontal");
        float ver = Input.GetAxis("Vertical");
        bool isMoving = false;

        Vector3 playerMovement = new Vector3(hor, 0f, ver) * speed * Time.deltaTime;
        transform.Translate(playerMovement, Space.Self);

        if (ver != 0 || hor != 0)
        {
            isMoving = true;
        }
        else if (ver == 0 && hor == 0)
        {
            isMoving = false;
        }

        myAnim.SetBool("isMoving", isMoving);
        myAnim.SetFloat("hor", hor);
        myAnim.SetFloat("ver", ver);

        //myAnim.SetFloat("mag", Input.GetAxis("Vertical"));
        //myAnim.SetFloat("hor", hor);
        //transform.Rotate(0f, hor * speedRot * Time.deltaTime, 0f);
    }
    
    //jarak player dengan objek yang di klik
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("objectClick"))
        {
            inRange = true;
            other.gameObject.transform.Find("Clickable").gameObject.SetActive(false);
            Animator myOtherAnim = other.gameObject.transform.Find("ChatIcon").GetComponent<Animator>();
            myOtherAnim.SetTrigger("OpenChat");
        }
        
        if (other.CompareTag("Booth"))
        {
            inBooth = true;
            //other.gameObject.transform.GetChild(0).gameObject.SetActive(true);
            Animator information = other.gameObject.transform.GetChild(0).gameObject.GetComponent<Animator>();
            information.SetBool("isInside", true);
        }

        if (other.CompareTag("Lift"))
        {
            inLift = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("objectClick"))
        {
            inRange = false;
            Animator myOtherAnim = other.gameObject.transform.Find("ChatIcon").GetComponent<Animator>();
            myOtherAnim.SetTrigger("CloseChat");
            other.gameObject.transform.Find("Clickable").gameObject.SetActive(true);
        }

        if (other.CompareTag("Booth"))
        {
            inBooth = false;
            //other.gameObject.transform.GetChild(0).gameObject.SetActive(false);
            //other.gameObject.transform.localScale = new Vector3(.5f, .5f, 1f);
            Animator information = other.gameObject.transform.GetChild(0).gameObject.GetComponent<Animator>();
            information.SetBool("isInside", false);
        }

        if (other.CompareTag("Lift"))
        {
            inLift = false;
        }
    }

    public void buttonFuntion(string _typeButton)
    {
        switch (_typeButton)
        {
            case "Close":
                inMenu = false;
                break;
        } 
    }

    public void WalkSound()
    {
        int selectSound = Random.Range(1, 5);       
        FindObjectOfType<AudioManager>().Play("Walk" + selectSound);
    }
}
