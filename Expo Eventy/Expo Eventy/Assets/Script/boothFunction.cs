﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class boothFunction : MonoBehaviour {

    #region ClassList
    [Serializable]
    public class Booth
    {
        public string boothName;
        public string url;

        public Sprite kartuNama;
        public Sprite information;
        public Sprite[] brosurs;
        public Sprite[] pictures;
    }        

    #endregion


    public string playerInteract;

    public Booth booths;
    public Image cardNameImg, brosurImg; 
  
}
