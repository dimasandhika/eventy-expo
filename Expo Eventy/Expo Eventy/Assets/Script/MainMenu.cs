﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public int index;

    public GameObject loadingScreen;
    public Slider slider;
    public Text progresTxt;

    void Start()
    {       
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        StartCoroutine(GetData());
    }

    
    public void buttonQuit()
    {
        Application.Quit();
    }

    public void buttonFunction(string _buttonType)
    {
        switch(_buttonType)
        {
            case "Login":
                SceneManager.LoadScene(1);
                break;
        }
    }

    public void loading(int _sceneIndex) 
    {
        StartCoroutine(Loadsychronously(_sceneIndex));
    }

    public void webRequest()
    {        
        // https://dog.ceo/api/breeds/image/random
    }

    IEnumerator Loadsychronously(int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        loadingScreen.SetActive(true);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);

            slider.value = progress;
            progresTxt.text = (int)progress * 100f + "%";

            yield return null;
        }
    }

    IEnumerator GetData()
    {
        //WWWForm form = new WWWForm();

        using (UnityWebRequest www = UnityWebRequest.Get("https://dog.ceo/api/breeds/image/random"))
        {
            yield return www.Send();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }  
        } 
    }
}
