﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class menuControl : MonoBehaviour {

    #region Class List
    [Serializable]
    public class MainPanel
    {
        [Serializable]
        public class TypePanel
        {
            public string name;
            public GameObject panel;
        }


        public TypePanel[] typePanels;
        int index;


        public void openPanel(string _name, bool _isActive)
        {
            for (int i = 0; i < typePanels.Length; i++)
            {
                if(_name == typePanels[i].name)
                {
                    index = i;
                    typePanels[index].panel.SetActive(_isActive);
                }
                else if (_name == "All")
                {
                    typePanels[i].panel.SetActive(_isActive);
                }
            }
        }
    }

    [Serializable]
    public class PanelPause
    {
        [Serializable]
        public class TypePanel
        {
            public string name;
            public GameObject panel;
        }

        public TypePanel[] typePanels;
        int index;

        public void openPanel(string _name, bool _isActive)
        {
            for (int i = 0; i < typePanels.Length; i++)
            {
                if (_name == typePanels[i].name)
                {
                    index = i;
                    typePanels[index].panel.SetActive(_isActive);
                }
                else if (_name == "All")
                {
                    typePanels[i].panel.SetActive(_isActive);
                }
            }
        }
    }

    [Serializable]
    public class PanelChat
    {
        [Serializable]
        public class TypePanel
        {
            public string name;
            public GameObject panel;
        }

        public TypePanel[] typePanels;
        int index;

        public void openPanel(string _name, bool _isActive)
        {
            for (int i = 0; i < typePanels.Length; i++)
            {
                if (_name == typePanels[i].name)
                {
                    index = i;
                    typePanels[index].panel.SetActive(_isActive);
                }
                else if (_name == "All")
                {
                    typePanels[i].panel.SetActive(_isActive);
                }
            }
        }
        
    }

    [Serializable]
    public class PanelMisc
    {
        [Serializable]
        public class TypePanel 
        {
            public string name;
            public GameObject panel;
        }

        public TypePanel[] typePanels;
        int index;

        public void openPanel(string _name, bool _isActive)
        {
            for (int i = 0; i < typePanels.Length; i++)
            {
                if (_name == typePanels[i].name)
                {
                    index = i;
                    typePanels[index].panel.SetActive(_isActive);
                }
                else if (_name == "All")
                {
                    typePanels[i].panel.SetActive(_isActive);
                }
            }
        }
    }  

    [Serializable]
    public class PanelUser
    {
        [Serializable]
        public class TypePanel
        {
            public string name;
            public GameObject panel;
        }

        public TypePanel[] typePanels;
        int index;

        public void openPanel(string _name, bool _isActive)
        {
            for (int i = 0; i < typePanels.Length; i++)
            {
                if(_name == typePanels[i].name)
                {
                    index = i;
                    typePanels[index].panel.SetActive(_isActive);
                }
                else if(_name == "All")
                {
                    typePanels[i].panel.SetActive(_isActive);
                }
            }
        }
    }


    #endregion

    [Header("Class List")]
    public MainPanel mainPanel;
    public PanelPause panelPause;
    public PanelChat panelChat;
    public PanelMisc panelMisc;
    public PanelUser panelUser;

    [Header("Tutorial")]
    public GameObject bgTutorial;
    public GameObject panelPeringatan;
    public GameObject panelInMenu;

    [Header("Booth")]
    public BoothType[] boothTypes;
    public Image cardNameImg;
    public Image brosurImg;
    public GameObject[] typePanel;
    public GameObject[] playerSetting;

    public GameObject loading;

    [Header("Informasi")]
    public Button[] tabButtons;

    int boothIndex, index;
    int limit;
    bool tabSwitch = true;

    [Header("NpcChat")]
    public NpcType[] npcTypes;
    public Text npcNameTxt;
    public Image npcPhoto;

    int npcIndex;

    [Header("InfoPanel")]
    public GameObject[] profileMenuPanel;
    //bool isOpen = false;
    

    [Header("PanelUser")]
    public InputField name, companyName, email, bio;
    public Text nameTxt, companyNameTxt, emailTxt, bioTxt;

    Animator npcProifileAnim, informationAnim; /*panelInfoAnim*/ 

    void Start()
    {
        npcProifileAnim = npcPhoto.gameObject.GetComponent<Animator>();
        informationAnim = typePanel[0].transform.GetChild(0).GetComponent<Animator>();
        //panelInfoAnim = typePanel[2].gameObject.GetComponent<Animator>();
    }

    void Update()
    {

        changeProfile();

        //panelInfoAnim.SetBool("isOpen", isOpen);

        //Membedakan boots dan kontent di dalam boot.
        cardNameImg.sprite = boothTypes[boothIndex].kartuNama;
        brosurImg.sprite = boothTypes[boothIndex].brosurs[index];
        limit = boothTypes[boothIndex].brosurs.Length;

        npcNameTxt.text = npcTypes[npcIndex].nameNpc;

        //Kondisi pada saat suatu gameobject aktif di hierarcy
        if (npcTypes[npcIndex].photoProfile != null)
        {
            npcPhoto.sprite = npcTypes[npcIndex].photoProfile;
        }
        
        //kondisi saat close animasi berakhir
        //panel akun.
        if (typePanel[1].GetComponent<Image>().enabled == false)
        {
            typePanel[1].gameObject.SetActive(false);
            typePanel[1].GetComponent<Image>().enabled = true;
            panelInMenu.SetActive(false);
        }

        //panel informasi
        if (typePanel[0].transform.GetChild(0).GetComponent<Image>().enabled == false)
        {
            typePanel[0].SetActive(false);
            typePanel[0].transform.GetChild(0).GetComponent<Image>().enabled = true;
            tabSwitch = true;            
            cardNameImg.transform.parent.gameObject.SetActive(false);
            brosurImg.transform.parent.gameObject.SetActive(false);
            panelInMenu.SetActive(false);
        }

        //kondisi di mana saat panel menu aktif, maka panel hud akan nonaktif.
        if (panelInMenu.activeInHierarchy == true && typePanel[0].activeInHierarchy == false)
        {
            mainPanel.openPanel("PanelPause", true);
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                panelInMenu.SetActive(false);
            }
        }
        else if (panelInMenu.activeInHierarchy == false && typePanel[0].activeInHierarchy == false)
        {
            mainPanel.openPanel("PanelPause", false);
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                panelInMenu.SetActive(true);
            }
        }

        //konidisi saat player mengklik url.
        if (loading.activeInHierarchy == true) 
        {
            Application.OpenURL(boothTypes[boothIndex].url);
            loading.SetActive(false);
        }

        //Kondisi dimana gambar brosur dibuat seperti semula setelah animasi berakhir.
        if (brosurImg.gameObject.activeInHierarchy == false)
        {
            brosurImg.gameObject.SetActive(true);
            brosurImg.rectTransform.sizeDelta = new Vector2 (295, 400);
        }

        //kondisi input saat panel menu aktif
        if (brosurImg.transform.parent.gameObject.activeInHierarchy == true)
        {
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                buttonInformation("Next");
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                buttonInformation("Previous");
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (typePanel[0].activeInHierarchy == true)
            {
                buttonInformation("ClosePanelInformasi");
            }
            else if (typePanel[1].activeInHierarchy == true)
            {
                buttonProfile("CloseNpcProfile");
            }

            for (int i = 0; i < profileMenuPanel.Length; i++)
            {
                if (profileMenuPanel[i].activeInHierarchy == true)
                {
                    buttonProfile("Close/Back");
                }
            }
        }
        
        if (typePanel[0].activeInHierarchy == true)
        {
            if (Input.GetKeyDown(KeyCode.Tab))
            {
                tabSwitch = !tabSwitch;
            }

            if (tabSwitch)
            {
                tabButtons[0].interactable = false;
                tabButtons[1].interactable = true;
                cardNameImg.transform.parent.gameObject.SetActive(true);
                brosurImg.transform.parent.gameObject.SetActive(false);
            }
            else
            {
                tabButtons[0].interactable = true;
                tabButtons[1].interactable = false;
                cardNameImg.transform.parent.gameObject.SetActive(false);
                brosurImg.transform.parent.gameObject.SetActive(true);
            }
        }       
    }

    public void showPanel(string _bootTypes, string _typePanel)
    {
        bool _openPanel = false;
        for (int i = 0; i < boothTypes.Length; i++)
        {
            if (_bootTypes == boothTypes[i].nameBooth)
            {
                Debug.Log("There is booth named " + _bootTypes);
                boothIndex = i;
                _openPanel = true;
            }
        }        

        if (_openPanel)
        {
            switch (_typePanel)
            {
                case "Informasi":
                    panelInMenu.SetActive(true);
                    typePanel[0].SetActive(true);
                    break;
                case "Eventy":
                    loading.SetActive(true);
                    break;
            }

            if (typePanel[0].activeInHierarchy == true)
            {
                typePanel[0].transform.GetChild(0).gameObject.SetActive(true);
                cardNameImg.transform.parent.gameObject.SetActive(true);
            }
            else
            {
                typePanel[0].transform.GetChild(0).gameObject.SetActive(false);
            }
        }
        else
        {
            Debug.Log("There is no booth named " + _bootTypes);
        }
    }

    public void showProfile(string _npcName)
    {
        panelInMenu.SetActive(true);
        typePanel[1].SetActive(true);
        for (int i = 0; i < npcTypes.Length; i++)
        {
            if (_npcName == npcTypes[i].nameNpc)
            {
                npcIndex = i;
            }
        }
    }

    public void buttonInformation(string _buttonType)
    {
        FindObjectOfType<AudioManager>().Play("Click");
        switch (_buttonType)
        {
            case "ClosePanelInformasi":
                if (cardNameImg.transform.parent.gameObject.activeInHierarchy == true)
                {
                    informationAnim.SetTrigger("CloseCardName");
                }
                else if (brosurImg.transform.parent.gameObject.activeInHierarchy == true)
                {
                    informationAnim.SetTrigger("CloseBrosur");
                }
                break;
            case "TabBrosur":
                tabSwitch = false;
                break;
            case "TabCardName":
                tabSwitch = true;
                break;
            case "Next":
                index++;
                break;
            case "Previous":
                index--;
                break;
        }

        if (index == limit)
        {
            index = 0;
        }
        else if (index == -1)
        {
            index = limit - 1;
        }
    }   

    public void buttonProfile(string _buttonType)
    {
        FindObjectOfType<AudioManager>().Play("Click");
        typePanel[1].SetActive(false);
        switch (_buttonType)
        {
            case "Chat":
                profileMenuPanel[0].SetActive(true);
                break;
            case "Close/Back":
                for (int i = 0; i < profileMenuPanel.Length; i++)
                {
                    profileMenuPanel[i].SetActive(false);
                    typePanel[1].SetActive(true);
                }
                break;
            case "OpenIcon":
                //panelInfoAnim.SetBool("isOpen", true);
                //isOpen = true;
                break;
            case "CloseIcon":
                //panelInfoAnim.SetBool("isOpen", false);
                //isOpen = false;
                break;
            case "CloseNpcProfile":
                npcProifileAnim.SetTrigger("closing");
                panelInMenu.SetActive(false);
                break;
        }
    }

    public void mainPanelFunction(string _buttonType)
    {
        switch (_buttonType)
        {            
            case "Close":
                mainPanel.openPanel("All", false);
                break;
            default :
                mainPanel.openPanel(_buttonType, true);
                break;
        }
    }

    public void panelPauseFunction(string _buttonType)
    {
        FindObjectOfType<AudioManager>().Play("Click");
        switch (_buttonType)
        {
            case "Close":
                panelPause.openPanel("All", false);
                break;
            default:
                panelPause.openPanel(_buttonType, true);
                break;
        }

    }

    public void panelChatFunction(string _buttonType)
    {
        switch (_buttonType)
        {
            case "Close":
                panelChat.openPanel("All", false);
                break;
            default:
                panelChat.openPanel(_buttonType, true);
                break;
        }
    }

    public void panelMiscFunction(string _buttonType)
    {
        switch (_buttonType)
        {
            case "Close":
                panelMisc.openPanel("All", false);
                break;
            default:
                panelMisc.openPanel(_buttonType, true);
                break;
        }
    }

    public void panelUserFunction(string _buttonType)
    {
        switch(_buttonType)
        {
            case "Cancel" :
                panelUser.openPanel("All", false);
                break;
            case "Confirm" :
                panelUser.openPanel("All", false);
                break;
            default:
                panelUser.openPanel(_buttonType, true);
                break;
        }
    }

    public void buttonWarning(string _buttonType)
    {
        FindObjectOfType<AudioManager>().Play("Click");
        switch (_buttonType)
        {
            case "OpenWarning":
                panelInMenu.SetActive(true);
                panelPeringatan.SetActive(true);
                break;
            case "CloseWarning":
                panelPeringatan.SetActive(false);
                break;
        }
    }

    public void saveProfile()
    {
        string _name, _companyName, _email, _bio;

        _name = name.text;
        _email = email.text;
        _companyName = companyName.text;
        _bio = bio.text;

        PlayerPrefs.SetString("Name", _name);
        PlayerPrefs.SetString("Email", _email);
        PlayerPrefs.SetString("CompanyName", _companyName);
        PlayerPrefs.SetString("Bio", _bio);               
    }

    public void changeProfile()
    {
        nameTxt.text = PlayerPrefs.GetString("Name");
        emailTxt.text = PlayerPrefs.GetString("Email");
        companyNameTxt.text = PlayerPrefs.GetString("CompanyName");
        bioTxt.text = PlayerPrefs.GetString("Bio");
    }
}
